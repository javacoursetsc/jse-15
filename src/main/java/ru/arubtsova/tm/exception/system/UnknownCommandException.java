package ru.arubtsova.tm.exception.system;

import ru.arubtsova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Unknown command...");
    }

}

package ru.arubtsova.tm.exception.system;

import ru.arubtsova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Unknown argument...");
    }

}

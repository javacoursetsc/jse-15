package ru.arubtsova.tm.service;

import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.service.IProjectTaskService;
import ru.arubtsova.tm.exception.empty.EmptyIdException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService  {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskToProject(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.bindTaskToProject(taskId, projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public Project removeProjectWithTasksById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}

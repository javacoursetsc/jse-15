package ru.arubtsova.tm.controller;

import ru.arubtsova.tm.api.controller.IProjectController;
import ru.arubtsova.tm.api.service.IProjectService;
import ru.arubtsova.tm.enumerated.Sort;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("Project List:");
        System.out.println("Enter Sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects = new ArrayList<>();
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("Project Create:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("Project Clear:");
        projectService.clear();
    }

    private void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Start Date: " + project.getDateStart());
        System.out.println("Finish Date: " + project.getDateFinish());
        System.out.println("Created: " + project.getCreated());
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProjectById() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        System.out.println("Project Overview:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

    @Override
    public void removeProjectById() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully updated");
    }

    @Override
    public void updateProjectById() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(id, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully updated");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectById() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByName() {
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectById() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishProjectByName() {
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("Project:");
        System.out.println("Enter Project Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Project Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        final Project projectStatusUpdate = projectService.changeProjectStatusByIndex(index, status);
        if (projectStatusUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project status was successfully updated");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Project Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        final Project projectStatusUpdate = projectService.changeProjectStatusById(id, status);
        if (projectStatusUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project status was successfully updated");
    }

    @Override
    public void changeProjectStatusByName() {
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        final Project projectStatusUpdate = projectService.changeProjectStatusByName(name, status);
        if (projectStatusUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project status was successfully updated");
    }

}

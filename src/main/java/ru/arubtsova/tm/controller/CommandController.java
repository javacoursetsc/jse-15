package ru.arubtsova.tm.controller;

import ru.arubtsova.tm.api.controller.ICommandController;
import ru.arubtsova.tm.api.service.ICommandService;
import ru.arubtsova.tm.model.Command;
import ru.arubtsova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("About:");
        System.out.println("Name: Anastasia Rubtsova");
        System.out.println("E-mail: Lafontana@mail.ru");
        System.out.println("Company: TSC");
    }

    @Override
    public void showVersion() {
        System.out.println("Version: 1.5.0");
    }

    @Override
    public void showSystemInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("System Information:");
        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.format(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        System.out.println("Available commands:");
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        System.out.println("Available arguments:");
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("Help:");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
